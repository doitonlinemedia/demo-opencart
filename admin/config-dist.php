<?php
// HTTP
define('HTTP_SERVER', 'https://demo-opencart.doit/admin/');
define('HTTP_CATALOG', 'https://demo-opencart.doit/');

// HTTPS
define('HTTPS_SERVER', 'https://demo-opencart.doit/admin/');
define('HTTPS_CATALOG', 'https://demo-opencart.doit/');

// DIR
define('DIR_APPLICATION', '/app/admin/');
define('DIR_SYSTEM', '/app/system/');
define('DIR_IMAGE', '/app/image/');
define('DIR_STORAGE', '/app/storage');
define('DIR_CATALOG', '/app/catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'database');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'lemp');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');
